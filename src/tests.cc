#include <iostream>
#include <stdlib.h>

#include "utils.hh"
#include "spot/twaalgos/hoa.hh"
#include <spot/twaalgos/isdet.hh>
#include "spot/twaalgos/complement.hh"
#include "spot/twaalgos/simulation.hh"
#include "spot/twacube_algos/convert.hh"

#include "spot/twaalgos/sccinfo.hh"
#include "spot/twaalgos/isdet.hh"

int main()
{
  const auto [twagraphs, bdddict] = load_all("data");
  const auto twacubes = convert_all(twagraphs);
  std::cout << "load " << twagraphs.size() << " automata\n";

  spot::scc_info si(twagraphs[0]);

  for (unsigned i = 0; i < twacubes.size(); ++i)
  {
    std::cout << "Check " << i << "... " << std::flush;

    if (twacubes[i]->num_edges() > 1000)
    {
      std::cout << "skip (too large)\n";
      continue;
    }

    auto check_twa = [](const spot::twa_graph_ptr& a1,
                    const spot::twa_graph_ptr& a2,
                    std::string msg)
    {
      if (a1->intersects(spot::complement(a2))
          || a2->intersects(spot::complement(a2)))
      {
        std::cout << "Failed (" << msg << ")\n";
        spot::print_hoa(std::cout, a2);
        spot::print_hoa(std::cout, a1);
        std::exit(1);
      }
    };

    auto check = [](const spot::twacube_ptr& a1,
                    const spot::twa_graph_ptr& a2,
                    std::string msg)
    {
      auto a = spot::twacube_to_twa(a1, a2->get_dict());

      if (a->intersects(spot::complement(a2))
          || a2->intersects(spot::complement(a)))
      {
        std::cout << "Failed (" << msg << ")\n";
        spot::print_hoa(std::cout, a2);
        spot::print_hoa(std::cout, a);
        std::exit(1);
      }
    };

    if (i == 31 || (i >= 49 && i <= 51) || i == 57 || i == 62)
    {
      std::cout << "skip (are_equivalent is too slow)\n";
      continue;
    }

    spot::twa_graph_ptr sim_twa = spot::reduce_direct_sim(twagraphs[i]);
    check_twa(sim_twa, twagraphs[i], "twagraphs");

    sim_twa = spot::reduce_direct_cosim(twagraphs[i]);
    check_twa(sim_twa, twagraphs[i], "cosim twagraphs");

    spot::twacube_ptr sim = spot::reduce_direct_sim(twacubes[i]);
    check(sim, twagraphs[i], "cube, no parallelism");

    sim = spot::reduce_direct_sim(twacubes[i], 1);
    check(sim, twagraphs[i], "parrallel, 1 thread");

    sim = spot::reduce_direct_sim(twacubes[i], 8);
    check(sim, twagraphs[i], "parrallel, 8 threads");

    assert(spot::is_deterministic(twagraphs[i]) == spot::is_deterministic(twacubes[i]));

    std::cout << "Ok !" << std::endl;
  }

  return 0;
}
