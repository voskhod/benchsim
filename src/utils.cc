#include "spot/parseaut/public.hh"
#include "spot/twacube_algos/convert.hh"
#include "spot/twaalgos/sccfilter.hh"
#include "utils.hh"

#include <algorithm>
#include <iostream>

namespace fs = std::filesystem;

std::pair<std::vector<spot::twa_graph_ptr>, spot::bdd_dict_ptr>
load_one(const fs::path& p)
{
  spot::bdd_dict_ptr dict = spot::make_bdd_dict();
  std::vector<spot::twa_graph_ptr> v;
  v.reserve(12);

  if (p.extension() == ".hoa")
  {
    spot::automaton_stream_parser parser(p);
    while (auto a = parser.parse(dict)->aut)
      v.push_back(spot::scc_filter(a));
  }
  else
    throw std::invalid_argument("Not a hoa file");

  return {v, dict};
}


std::pair<std::vector<spot::twa_graph_ptr>, spot::bdd_dict_ptr>
load_all(const std::string& data_dir)
{
  spot::bdd_dict_ptr dict = spot::make_bdd_dict();
  std::vector<spot::twa_graph_ptr> v;
  v.reserve(200);

  for(const auto& p: fs::recursive_directory_iterator(data_dir))
  {
    if (p.is_regular_file() && p.path().extension() == ".hoa")
    {
      spot::automaton_stream_parser parser(p.path());
      while (auto a = parser.parse(dict)->aut)
        v.push_back(spot::scc_filter(a));
    }
  }

  return {v, dict};
}

std::vector<spot::twacube_ptr>
convert_all(const std::vector<spot::twa_graph_ptr>& twagraphs)
{
  std::vector<spot::twacube_ptr> twacubes(twagraphs.size());

  std::transform(twagraphs.begin(), twagraphs.end(),
                  twacubes.begin(), spot::twa_to_twacube);

  return twacubes;
}
