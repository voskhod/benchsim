#pragma once

#include <filesystem>
#include <string>
#include <vector>

#include "spot/twa/bdddict.hh"
#include "spot/twa/twagraph.hh"
#include "spot/twacube/twacube.hh"

std::pair<std::vector<spot::twa_graph_ptr>, spot::bdd_dict_ptr>
load_one(const std::filesystem::path& data_dir);

std::pair<std::vector<spot::twa_graph_ptr>, spot::bdd_dict_ptr>
load_all(const std::string& data_dir);

std::vector<spot::twacube_ptr>
convert_all(const std::vector<spot::twa_graph_ptr>& twagraphs);
