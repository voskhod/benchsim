#include <benchmark/benchmark.h>
#include <iostream>

#include "utils.hh"
#include "spot/twaalgos/simulation.hh"

const auto twacubes = []()
{
  std::string path = std::getenv("BENCH");
  auto [twagraphs, _] = load_one(path);

  if (twagraphs.size() == 0)
  {
    std::cerr << "Empty file: " << path << ", exit\n";
    std::exit(0);
  }

  return convert_all(twagraphs);
}();

static void sim(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  for (auto _ : state)
  {
    const auto& a = twacubes[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();

    auto aa = spot::reduce_direct_sim(a);

    rs = aa->num_states();
    re = aa->num_edges();

    benchmark::ClobberMemory();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
}

static void simPar1(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  for (auto _ : state)
  {
    const auto& a = twacubes[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();

    auto aa = spot::reduce_direct_sim(a, 1);

    rs = aa->num_states();
    re = aa->num_edges();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
}

static void simPar4(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  for (auto _ : state)
  {
    const auto& a = twacubes[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();

    auto aa = spot::reduce_direct_sim(a, 4);

    rs = aa->num_states();
    re = aa->num_edges();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
}

static void simPar8(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  for (auto _ : state)
  {
    const auto& a = twacubes[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();

    auto aa = spot::reduce_direct_sim(a, 8);

    rs = aa->num_states();
    re = aa->num_edges();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
}

BENCHMARK(sim)->DenseRange(0, twacubes.size() - 1);
//BENCHMARK(simPar1)->DenseRange(0, twacubes.size() - 1);
//BENCHMARK(simPar4)->DenseRange(0, twacubes.size() - 1);
//BENCHMARK(simPar8)->DenseRange(0, twacubes.size() - 1);

BENCHMARK_MAIN();
