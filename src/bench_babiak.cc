#include <benchmark/benchmark.h>
#include <iostream>

#include "utils.hh"
#include "spot/twaalgos/hoa.hh"
#include "spot/twaalgos/isdet.hh"
#include "spot/twaalgos/simulation.hh"

const auto twagraphs = []()
{
  std::string path = std::getenv("BENCH");
  auto [twagraphs, _] = load_one(path);

  if (twagraphs.size() == 0)
  {
    std::cerr << "Empty file: " << path << ", exit\n";
    std::exit(0);
  }

  std::vector<spot::twa_graph_ptr> v;
  for (auto a : twagraphs)
    if (a->num_edges() <= 2000)
      v.push_back(a);

  return v;
}();

static void sim(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  unsigned d = 0, rd = 0;

  for (auto _ : state)
  {
    const auto& a = twagraphs[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();
    d = spot::count_nondet_states(a);

    auto aa = spot::reduce_iterated(a);

    if (aa)
    {
      rs = aa->num_states();
      re = aa->num_edges();
      rd = spot::count_nondet_states(aa);
    }
    else
    {
      rs = a->num_states();
      re = a->num_edges();
      rd = spot::count_nondet_states(aa);
    }

    benchmark::ClobberMemory();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
  state.counters["det"] = d;
  state.counters["red_det"] = rd;
}

static void babiak(benchmark::State& state)
{
  unsigned ns = 0, ne = 0;
  unsigned rs = 0, re = 0;
  unsigned d = 0, rd = 0;

  for (auto _ : state)
  {
    const auto& a = twagraphs[state.range(0)];
    ns = a->num_states();
    ne = a->num_edges();
    d = spot::count_nondet_states(a);

    auto aa = spot::iterated_simulations(a);

    if (aa)
    {
      rs = aa->num_states();
      re = aa->num_edges();
      rd = spot::count_nondet_states(aa);
    }
    else
    {
      rs = a->num_states();
      re = a->num_edges();
      rd = spot::count_nondet_states(aa);
    }

    if (ne == 13 && re == 1)
      spot::print_hoa(std::cerr, a);

    benchmark::ClobberMemory();
  }

  state.counters["states"] = ns;
  state.counters["edges"] = ne;
  state.counters["red_states"] = rs;
  state.counters["red_edges"] = re;
  state.counters["det"] = d;
  state.counters["red_det"] = rd;
}


BENCHMARK(sim)->DenseRange(0, twagraphs.size() - 1);
BENCHMARK(babiak)->DenseRange(0, twagraphs.size() - 1);

BENCHMARK_MAIN();
