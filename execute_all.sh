#!/bin/bash

[ $# != 3 ] && echo 'Missing args: expect ./execute_all.sh $libspot_dir $data_path' && exit 0

libspot_dir="$1"
data_path="$2"
bench="$3"

progress_bar() {
  local width=20
  to_draw=$(($1 * width / $2))
  space=$((width - to_draw))

  printf '['
  printf '#%.s' $(eval "echo {0.."$(($to_draw))"}")

  if [ "$space" != "0" ]; then
    printf ' %.s' $(eval "echo {0.."$(($space))"}")
  fi

  printf "] $(($1 * 100 / $2))%%\r"
}

number=0
result_dir='results-00'
while [ -e "$result_dir" ]; do
    printf -v result_dir '%s-%02d' results "$(( ++number ))"
done

mkdir "$result_dir"

cat /proc/cpuinfo >> "$result_dir"/log.txt

echo "Computing genltl..."
echo -e "Computing genltl\n\n" >> "$result_dir"/log.txt

nb_aut=$(ls "$data_path/genltl/"*.hoa | wc -l)
cur=0

for aut_path in $(ls $"$data_path"/genltl/*.hoa)
do
  progress_bar cur nb_aut
  f=$(basename -- "$aut_path")
  result="${f%.*}"

  LD_LIBRARY_PATH="$libspot_dir" BENCH="$aut_path" ./"$bench" \
      --benchmark_format=csv 2>> "$result_dir"/log.txt 1> "$result_dir/""$result".csv

  if [ "$TERM" == "kitty" ] && [ "$bench" == "bench" ]
  then
    ./plot.py "$result_dir/""$result".csv | kitty icat --align=left
  fi

  cur=$((cur + 1))
done

progress_bar cur nb_aut
# Do not rewrite on the progress bar
echo ""
echo ""



echo "Computing spin.13..."
echo -e "Computing spin.13l\n\n" >> "$result_dir"/log.txt

nb_aut=$(ls "$data_path/spin.13/"*.hoa | wc -l)
cur=0

for aut_path in $(ls $"$data_path"/spin.13/*.hoa)
do
  progress_bar cur nb_aut
  f=$(basename -- "$aut_path")
  result="${f%.*}"

  LD_LIBRARY_PATH="$libspot_dir" BENCH="$aut_path" ./"$bench" \
      --benchmark_format=csv 2>> "$result_dir"/log.txt 1> "$result_dir/""$result".csv

  if [ "$TERM" == "kitty" ] && [ "$bench" == "bench" ]
  then
    ./plot.py "$result_dir/""$result".csv | kitty icat --align=left
  fi

  cur=$((cur + 1))
done



progress_bar cur nb_aut
# Do not rewrite on the progress bar
echo ""
echo ""

first="true"
for dir in circuits rabbit
do
  echo "Computing $dir..."
  echo -e "Computing $dir\n\n" >> "$result_dir"/log.txt

  nb_aut=$(ls "$data_path/$dir/"*.hoa | wc -l)
  cur=0

  for aut_path in $(ls $"$data_path"/"$dir"/*.hoa)
  do
    progress_bar cur nb_aut

    LD_LIBRARY_PATH="$libspot_dir" BENCH="$aut_path" ./"$bench" \
        --benchmark_format=csv 2>> "$result_dir"/log.txt 1> "$result_dir"/tmp.csv

    # Remove the header if it's not the first bench
    if [ "$first" == "true" ]
    then
      cat "$result_dir"/tmp.csv > "$result_dir"/misc.csv
      first=false
    else
      tail -n +2 "$result_dir"/tmp.csv >> "$result_dir"/misc.csv
    fi

    cur=$((cur + 1))
  done

  progress_bar cur nb_aut

  # Same as above
  echo ""
  echo ""
done

rm "$result_dir"/tmp.csv

echo "Done ! Results saved in $result_dir"
