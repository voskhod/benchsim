#!/bin/python3

from io import StringIO
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import glob
import math
import seaborn as sns
from matplotlib import cm

from matplotlib import cycler
colors = cycler('color',
                ['#EE6666', '#3388BB', '#9988DD', '#EECC55', '#88BB44', '#FFBBBB'])
plt.rc('axes', facecolor='#E6E6E6', edgecolor='none',
              axisbelow=True, grid=True, prop_cycle=colors)
plt.rc('grid', color='w', linestyle='solid')
plt.rc('xtick', direction='out', color='#e6d6ac')
plt.rc('ytick', direction='out', color='#e6d6ac')
plt.rc('axes', labelcolor='#e6d6ac')
plt.rc('patch', edgecolor='#E6E6E6')
plt.rc('patch', facecolor='blue')
plt.rc('lines', linewidth=2)

matplotlib.rcParams['text.color'] = '#e6d6ac'

def load_bench(path):
    df = pd.read_csv(path, comment='#')
    df.dropna(axis=1, how='all', inplace=True)
    df.drop('time_unit', axis=1, inplace=True)
    df['deg'] = df['edges'] / df['states']
    return df

def show_bench(bench, ax, nb_bench=4, x='edges', name=None, version=''):
    nb_aut = len(bench) // nb_bench

    time_unit = 'ns'
    factor = 1

    med = bench['real_time'].mean()

    if med > 10**9:
        time_unit = 's'
        factor = 10**-9
    elif med > 10**6:
        time_unit = 'µs'
        factor = 10**-6

    for i in range(nb_bench):
        begin = i*nb_aut

        if name != None and name != bench['name'][begin][:-2]:
            continue

        end = (i+1)*nb_aut
        ax.plot(bench[begin:end][x], bench[begin:end]['real_time'] * factor, \
                "-o", label=version + bench['name'][begin][:-2])

        ax.set_xlabel(x)
        ax.set_ylabel('time('+ time_unit + ')')
        plt.title(sys.argv[1][:-4])

if len(sys.argv) != 2:
    print('Usage: ./plot.py bench.csv')
    exit(1)

bench = load_bench(sys.argv[1])

fig = plt.figure()
fig.patch.set_facecolor('#392f32')
ax = fig.add_subplot(111)
show_bench(bench, ax)
legend = plt.legend()
plt.setp(legend.get_texts(), color='black')

f = StringIO()
fig.savefig(f, format='svg')
print(f.getvalue())
