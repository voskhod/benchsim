# Data

* circuis: benchmarks from ism and mcnc in hoa.
* rabbit: examples from [rabbit](http://www.languageinclusion.org/doku.php?id=tools).
* genltl: automate genered by `genltl $PATTERN | ltl2tgba --low --any`.

To generate them, do `make data`. This must be done before running the tests
and the benchmarks.

# Tests

* Run `make check`

# Bencharks

* Run `make time $BENCH` where `BENCH` is a path to valid hoa file.
* Or run `make timeall` to run all the benchs from data directory.
* Then visualize the using the `visu_bench.ipynb` notebook.

# Results

* par1:   All threads share a TODO global queue
* par2:   Partition, shared TODO
* par2_1: Partition, local TODO
* par3:   No partition, random order
* par4:   All threads compute all simulation with a different topological order.
* par4_2: Same as par4 but with a better update.

* deter:  Improved determinism

