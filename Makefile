CC=g++
CPPFLAGS=-I.. -I../buddy/src
CFLAGS=-Wall -Wextra -pedantic -std=c++17
LDFLAGS=-L${LIBSPOT_DIR} -lspot

TESTS_SRC=src/utils.cc src/tests.cc
BENCH_SRC=src/utils.cc src/bench.cc
BENCH_BABIAK_SRC=src/utils.cc src/bench_babiak.cc
LIBSPOT_DIR=../spot/.libs
DATA_DIR=data
SPOT_SRC=$(shell ls ../spot/*/*.cc ../spot/*/*.hh)

ifeq (time,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  AUT_PATH := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

ifeq (prof,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  AUT_PATH := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif



all: ${SPOT_SRC}
	cd .. && make -j8; cd benchsim
	touch all

.PHONY: data
data:
	cd data && ./generate_bench_data.sh && cd ..

tests: ${TESTS_SRC} all
	${CC} ${TESTS_SRC} ${CPPFLAGS} ${CFLAGS} -Og -g -fsanitize=address ${LDFLAGS} -o tests

.PHONY: check
check: tests
	LD_LIBRARY_PATH=${LIBSPOT_DIR} ./tests

bench: ${BENCH_SRC} all
	${CC} ${BENCH_SRC} ${CPPFLAGS} ${CFLAGS} -O3 ${LDFLAGS} -lpthread -lbenchmark -o bench

bench_babiak: ${BENCH_BABIAK_SRC} all
	${CC} ${BENCH_BABIAK_SRC} ${CPPFLAGS} ${CFLAGS} -O3 ${LDFLAGS} -lpthread -lbenchmark -o bench_babiak

.PHONY: time
time: bench
	sudo cpupower frequency-set --governor performance
	LD_LIBRARY_PATH=${LIBSPOT_DIR} BENCH=${AUT_PATH} ./bench
	sudo cpupower frequency-set --governor powersave

.PHONY: prof
prof: bench
	sudo cpupower frequency-set --governor performance
	LD_LIBRARY_PATH=${LIBSPOT_DIR} BENCH=${AUT_PATH} perf record ./bench
	sudo cpupower frequency-set --governor powersave

.PHONY: timeall
timeall: bench
	sudo cpupower frequency-set --governor performance
	./execute_all.sh ${LIBSPOT_DIR} ${DATA_DIR} bench
	sudo cpupower frequency-set --governor powersave

.PHONY: timeall_babiak
timeall_babiak: bench_babiak
	sudo cpupower frequency-set --governor performance
	./execute_all.sh ${LIBSPOT_DIR} ${DATA_DIR} bench_babiak
	sudo cpupower frequency-set --governor powersave

.PHONY: visu
visu:
	cd .. && ./tests/run jupyter notebook benchsim/visu_bench.ipynb

.PHONY: restore
restore:
	sudo cpupower frequency-set --governor powersave
