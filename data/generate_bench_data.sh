#!/bin/bash -e

# Convert the rabbit automata from ba to hoa
echo "Converting automata from Rabbit"

cd rabbit/
./ba_to_hoa.py *.ba
cd ..

# Generate automata with genltl | ltl2tgba
echo "Generating automata from random LTL fromulas"

mkdir -p genltl
cd genltl
path_to_bin='../../../bin'

for pattern in "rv-counter=1..12" "ccj-alpha=1..12" "tv-uu=1..12" "kr-n=1..4" "ccj-beta-prime=1..12" "pps-arbiter-standard=1..4"
do
  filename=${pattern//=/_}
  "$path_to_bin"/genltl --"$pattern" | "$path_to_bin"/ltl2tgba --low --any > "$filename".hoa
done
cd ..

# Fetch data ans translate formaula from spin 13
echo "Generating automata from spin'13 LTL fromulas"

cd spin.13

wget https://www.lrde.epita.fr/~adl/spin13/2013-03-12.tar.xz &> /dev/null
tar xvf 2013-03-12.tar.xz &> /dev/null
rm -f ls *.tar.xz *.csv *.tex *.pdf *.log *.html

for f in $(ls *.json); do
  ../../../tests/run python json_ltl_to_tgba.py "$f"
done

cd ..
