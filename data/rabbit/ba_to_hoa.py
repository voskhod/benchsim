#!/bin/python3

import sys
import re
import math

m = 32

class automaton:
    def __init__(self):
        self.initial_state = None
        # (src, dst, cond)
        self.edges = []
        self.accepting_states = []
        self.num_states = 0
        self.num_ap = 0

filename = sys.argv[1]

def parse_file(filename):
    res = automaton();
    states_label = {}
    cond_label = {}

    regex = '[,\->]+'

    def get_states(label):
        if not label in states_label:
            states_label[label] = res.num_states
            res.num_states += 1
            res.edges.append([])
        return states_label[label]

    def get_cond(label):
        if not label in cond_label:
            cond_label[label] = res.num_ap
            res.num_ap += 1

        return cond_label[label]



    with open(filename, 'r') as f:
        try:
            lines = iter(f.readlines())
            tokens = re.split(regex, next(lines)[:-1])

            # Check if the first line is the initial_state or an edges
            if (len(tokens) == 1):
                res.initial_state = get_states(tokens[0])
                tokens = re.split(regex, next(lines)[:-1])
            else:
                res.initial_state = get_states(tokens[1])

            # Parse the edges
            while (len(tokens) == 3):
                src = get_states(tokens[1])
                dst = get_states(tokens[2])
                cond = get_cond(tokens[0])

                res.edges[src].append((dst, cond))
                tokens = re.split(regex, next(lines)[:-1])

            while (len(tokens) != 0):
                res.accepting_states.append(get_states(tokens[0]))
                tokens = re.split(regex, next(lines)[:-1])

        except StopIteration:
            pass

    return res

def dump_to_hoa(aut, filename):

    m = math.ceil(math.log2(int(aut.num_ap)))

    def to_ap(n):
        s = ''


        for i in range(m):
            if (n & (1 << i)) == 1:
                s += str(i);
            else:
                s += '!' + str(i)

            if i != math.ceil(m) - 1:
                s += ' & '

        return s


    with open(filename, 'w') as f:
        f.write('HOA: v1\n')
        f.write('States: ' + str(aut.num_states) + '\n')
        f.write('Start: ' + str(aut.initial_state) + '\n')

        f.write('AP: ' + str(m))
        for i in range(m):
            f.write(' ' + '\"' + str(i) + '\"')
        f.write('\n')

        if len(aut.accepting_states) == 0:
            f.write('Acceptance: t\n')
        else:
            f.write('Acceptance: 1 Inf(0)\n')

        f.write('properties: trans-labels explicit-labels state-acc\n')
        f.write('--BODY--\n')

        for src in range(aut.num_states):
            if src in aut.accepting_states:
                f.write('State: ' + str(src) + '{0}\n')
            else:
                f.write('State: ' + str(src) + '\n')

            for (dst, cond) in aut.edges[src]:
                f.write('[' + to_ap(cond) + '] ' + str(dst) + '\n')

        f.write('--END--\n')


if len(sys.argv) == 1:
    sys.exit(1)

for input_file in sys.argv[1:]:
    try:
        aut = parse_file(input_file)

        i = input_file.rfind('ba')
        out_file = input_file[:i] +  'hoa'

        dump_to_hoa(aut, out_file)
    except Exception as e:
        print(input_file, 'parsing failed:', e)


with open('rabbit.py', 'w') as outfile:
    outfile.write('import spot\n\n')
    outfile.write('rabbit_aut = []\n\n')

    for input_file in sys.argv[1:]:
        i = input_file.rfind('ba')
        input_file = input_file[:i] +  'hoa'

        outfile.write('aut = spot.automaton(\'\'\'')
        with open(input_file) as infile:
            for line in infile:
                outfile.write(line)
        outfile.write('\'\'\')\n')
        outfile.write('rabbit_aut.append(aut)\n\n')

