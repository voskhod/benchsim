import json
import sys
import spot

if len(sys.argv) != 2:
    print('Usage: json_ltl_to_tgba.py JSON_FILE')
    sys.exit(1)

inputs = json.load(open(sys.argv[1]))['inputs']


outputs = open(sys.argv[1][:-4] + 'hoa', 'w')
for ltl in inputs:
    aut = spot.translate(ltl, 'low', 'any')
    outputs.write(aut.to_str('hoa'))

